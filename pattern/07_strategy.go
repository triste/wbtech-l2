package pattern

import "fmt"

/*
	Реализовать паттерн «стратегия».
Объяснить применимость паттерна, его плюсы и минусы, а также реальные примеры использования данного примера на практике.
	https://en.wikipedia.org/wiki/Strategy_pattern
*/

type Cache struct {
	vals    []int
	evictor Evictor
}

func NewCache(e Evictor) *Cache {
	return &Cache{
		evictor: e,
	}
}

func (c *Cache) SetEvictorAlgorithm(e Evictor) {
	c.evictor = e
}

func (c *Cache) Add(val int) {
	if len(c.vals) > 0 {
		c.evictor.Evict(c)
	}
	c.vals = append(c.vals, val)
}

// Стратегия позволяет определить семейство алгоритмов, реализовать их в
// отдельных объектах, сделав их взаимозаменяемыми.
//
// Плюсы:
//   - замена алгоритмов на лету
//   - изоляция реализации алгоритмов от использующего их кода
//   - добавление новых алгоритмов без изменения их использующего кода
//
// Минусы:
//   - для каждого алгоритма нужно создавать класс
type Evictor interface {
	Evict(c *Cache)
}

type EvictorLRU struct {
}

func (e *EvictorLRU) Evict(c *Cache) {
	fmt.Println("evicting by rlu strategy")
}

type EvictorLFU struct {
}

func (e *EvictorLFU) Evict(c *Cache) {
	fmt.Println("evicting by lfu strategy")
}

func strategyClient() {
	lru := &EvictorLRU{}
	lfu := &EvictorLFU{}
	cache := NewCache(lru)
	cache.Add(0)
	cache.Add(1)
	cache.SetEvictorAlgorithm(lfu)
	cache.Add(2)
}
