package pattern

import (
	"fmt"
	"os"
)

/*
	Реализовать паттерн «посетитель».
Объяснить применимость паттерна, его плюсы и минусы, а также реальные примеры использования данного примера на практике.
	https://en.wikipedia.org/wiki/Visitor_pattern
*/

type Player struct {
	health int
}

type Box struct {
	static bool
}

// Посетитель разделяет алгоритмы от объектов над которыми они выполняют
// операции.
//
// Плюсы:
//   - расширение функционала объекта без его изменения
//   - новый функционал находится в одном объекте
//
// Минусы:
//   - обновление всех посетителей при добавлении/удалении типов посещаемых
//     объектов
//   - посетитель может не иметь необходимых прав для доступа к приватным полям
//     посещаемого объекта
type Visitor interface {
	VisitPlayer(p *Player)
	VisitBox(b *Box)
}

type Node interface {
	Accept(v Visitor)
}

func (p *Player) Accept(v Visitor) {
	v.VisitPlayer(p)
}

func (b *Box) Accept(v Visitor) {
	v.VisitBox(b)
}

type Saver struct {
	out *os.File
}

func NewSaver() Saver {
	return Saver{
		out: os.Stdout,
	}
}

func (s *Saver) VisitPlayer(p *Player) {
	fmt.Fprintf(s.out, "player health %v saved\n", p.health)
}

func (s *Saver) VisitBox(b *Box) {
	box := "box"
	if b.static {
		box += " static"
	}
	fmt.Fprintf(s.out, "%v saved\n", box)
}

func visitorClient() {
	saver := NewSaver()
	nodes := []Node{
		&Player{
			health: 100,
		},
		&Box{
			static: true,
		},
	}
	for _, node := range nodes {
		node.Accept(&saver)
	}
}
