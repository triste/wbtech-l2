package pattern

import (
	"fmt"
	"strings"
)

/*
	Реализовать паттерн «комманда».
Объяснить применимость паттерна, его плюсы и минусы, а также реальные примеры использования данного примера на практике.
	https://en.wikipedia.org/wiki/Command_pattern
*/

// Команда сохраняет запрос и его аргументы, позволяя передавать все необходимые
// данные для выполнения запроса через аргументы методов
//
// Плюсы:
//   - убирает зависимость между создающими операции объектами и объектами
//     вызывающие эти операции
//   - простая реализация отмены операции
//   - отложенный запуск операций
//
// Минусы:
//   - под каждую команду необходимо создавать дополнительный класс
type Command interface {
	Execute()
}

type EchoCommand struct {
	args []string
}

func (c *EchoCommand) Execute() {
	fmt.Println("ECHO: ", strings.Join(c.args, " "))
}

type ExecCommand struct {
	name string
	args []string
}

func (c *ExecCommand) Execute() {
	fmt.Printf("SHELL: [%v], %v\n", c.name, strings.Join(c.args, " "))
}

func commandClient() {
	commands := []Command{
		&ExecCommand{
			name: "ls",
			args: []string{"-al"},
		},
		&EchoCommand{
			args: []string{"hello", "world"},
		},
	}
	for _, cmd := range commands {
		cmd.Execute()
	}
}
