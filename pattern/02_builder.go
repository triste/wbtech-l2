package pattern

import "fmt"

/*
	Реализовать паттерн «строитель».
Объяснить применимость паттерна, его плюсы и минусы, а также реальные примеры использования данного примера на практике.
	https://en.wikipedia.org/wiki/Builder_pattern
*/

// Строитель позволяет конструировать шаг за шагом сложные объекты различных
// типов используя один и тот же код
//
// Плюсы:
//   - конструирование объекты шаг за шагом
//   - переиспользования кода конструирования для создания различных объектов
//   - разделение логики построения и бизнесс логики
//
// Минусы:
//   - необходимость создавать дополнительные классы для каждого продукта
type UIBuilder interface {
	AddButton(x, y int, name string)
	AddField(x, y int)
}

type TUI struct {
}

type TextBasedUIBuilder struct {
}

func (b *TextBasedUIBuilder) AddButton(x, y int, name string) {
	fmt.Printf("ncurses add_button(%v, %v, %v)\n", x, y, name)
}

func (b *TextBasedUIBuilder) AddField(x, y int) {
	fmt.Printf("ncurses add_field(%v, %v)\n", x, y)
}

func (b *TextBasedUIBuilder) Build() TUI {
	return TUI{}
}

type GUI struct {
}

type GraphicalUIBuilder struct {
}

func (b *GraphicalUIBuilder) AddButton(x, y int, name string) {
	fmt.Printf("QT AddButton(%v, %v, %v)\n", x, y, name)
}

func (b *GraphicalUIBuilder) AddField(x, y int) {
	fmt.Printf("QT AddField(%v, %v)\n", x, y)
}

func (b *GraphicalUIBuilder) Build() GUI {
	return GUI{}
}

func builderClient(builder UIBuilder) {
	builder.AddField(0, 10)
	builder.AddButton(20, 10, "Submit 1")
	builder.AddField(0, 100)
	builder.AddButton(20, 100, "Submit 2")
}
