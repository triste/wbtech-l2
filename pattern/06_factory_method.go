package pattern

import "fmt"

/*
	Реализовать паттерн «фабричный метод».
Объяснить применимость паттерна, его плюсы и минусы, а также реальные примеры использования данного примера на практике.
	https://en.wikipedia.org/wiki/Factory_method_pattern
*/

type Renderer interface {
	Draw(obj string)
}

type RendererVulkan struct {
}

func (r *RendererVulkan) Draw(obj string) {
	fmt.Println("vulkan drawcall: ", obj)
}

type RendererOpengl struct {
}

func (r *RendererOpengl) Draw(obj string) {
	fmt.Println("opengl drawcall: ", obj)
}

// Простая фабрика позволяет создавать объекты различных типов без их явного
// указания
//
// Плюсы:
//   - избавляет от привязки к конкретным объектам продуктов
//   - выносит создание продуктов в один метод
func NewRenderer(name string) (Renderer, error) {
	switch name {
	case "Vulkan":
		return &RendererVulkan{}, nil
	case "OpenGL":
		return &RendererOpengl{}, nil
	default:
		return nil, fmt.Errorf("Wrong renderer")
	}
}

func factoryMethodClient() {
	vulkan, _ := NewRenderer("vulkan")
	opengl, _ := NewRenderer("opengl")
	vulkan.Draw("triangle 0,1,2")
	opengl.Draw("triangle 2,1,4")
}
