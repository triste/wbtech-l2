package pattern

import (
	"fmt"
)

/*
	Реализовать паттерн «состояние».
Объяснить применимость паттерна, его плюсы и минусы, а также реальные примеры использования данного примера на практике.
	https://en.wikipedia.org/wiki/State_pattern
*/

type RenderPass interface {
	Draw(bindata string)
}

type RenderPassDeferred struct {
}

func (r *RenderPassDeferred) Draw(verticies string) {
	fmt.Printf("drawing verticies %v to gbufs\n", verticies)
}

type RenderPassLighting struct {
}

func (r *RenderPassLighting) Draw(lightPositions string) {
	fmt.Printf("lights processing with positions %v\n", lightPositions)
}

// Паттерн позволяет менять своё поведение на основании своего состояния
//
// Плюсы:
// - каждое состояние находится в своём классе и не зависит от других
// - добавление новых состояний не требует изменений в остальных
//
// Минусы:
// - оверинжениринг в случае когда состояний мало и/или меняются не часто
type RendererBackend struct {
	pass RenderPass
}

func (r *RendererBackend) Draw(data string) {
	r.pass.Draw(data)
}

func (r *RendererBackend) SetPass(pass RenderPass) {
	r.pass = pass
}

func NewRendererBackend(initPass RenderPass) *RendererBackend {
	return &RendererBackend{
		pass: initPass,
	}
}

func clientState() {
	defferred := &RenderPassDeferred{}
	rback := NewRendererBackend(defferred)
	rback.Draw("0/2/4, 1/4/3, 0/3/4")

	lighting := &RenderPassLighting{}
	rback.SetPass(lighting)
	rback.Draw("pos1, pos2, pos3")
}
