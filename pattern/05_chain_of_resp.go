package pattern

import (
	"fmt"
	"strings"
)

/*
	Реализовать паттерн «цепочка вызовов».
Объяснить применимость паттерна, его плюсы и минусы, а также реальные примеры использования данного примера на практике.
	https://en.wikipedia.org/wiki/Chain-of-responsibility_pattern
*/

// Цепочка обязанностей позволяет передавать запросы по цепочке обработчиков, в
// которых решается, передавать ли запрос дальше
//
// Плюсы:
//   - возможность менять порядок обработчиков
//   - расширения функционала с помощью добавления нового обработчика не изменяя
//     остальные
//
// Минусы:
//   - запросы могут остаться необработанными в случае если один из обработчиков
//     не передаёт запрос дальше по цепочке
type Handler interface {
	Handle(req string)
	SetNext(h Handler)
}

type BaseHandler struct {
	next Handler
}

func (b *BaseHandler) SetNext(h Handler) {
	b.next = h
}

type Controller struct {
	BaseHandler
}

func (c *Controller) Handle(req string) {
	if !strings.HasPrefix(req, "GET") {
		fmt.Println("controller: invalid method, abort")
		return
	}
	fmt.Println("controller: done")
	if c.next != nil {
		c.next.Handle(req)
	}
}

type Logger struct {
	BaseHandler
}

func (l *Logger) Handle(req string) {
	fmt.Println("logger: ", req)
	if l.next != nil {
		l.next.Handle(req)
	}
}

type Mapper struct {
	BaseHandler
}

func (l *Mapper) Handle(req string) {
	fmt.Printf("mapper: %v -> %v\n", req, strings.TrimPrefix(req, "GET"))
	if l.next != nil {
		l.next.Handle(req)
	}
}

func handlerClient() {
	logger := &Logger{}
	ctrl := &Controller{}
	mapper := &Mapper{}
	logger.SetNext(ctrl)
	ctrl.SetNext(mapper)
	logger.Handle("GET /orders")
	logger.Handle("POST /orders")
}
