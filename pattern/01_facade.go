package pattern

/*
	Реализовать паттерн «фасад».
Объяснить применимость паттерна, его плюсы и минусы,а также реальные примеры использования данного примера на практике.
	https://en.wikipedia.org/wiki/Facade_pattern
*/

import (
	"errors"
	"fmt"
)

type Validator struct {
}

func (v *Validator) Validate(id int) bool {
	if id%2 == 0 {
		return true
	}
	return false
}

type Repository struct {
	m map[int]int
}

func (r *Repository) GetByID(id int) (int, bool) {
	val, ok := r.m[id]
	return val, ok
}

func (r *Repository) Add(id int, val int) error {
	if _, ok := r.m[id]; ok {
		return errors.New("already exists")
	}
	r.m[id] = val
	return nil
}

type Notificator struct {
}

func (n *Notificator) Notify(str string) {
	fmt.Println(str)
}

// Фасад инкапсулирует сложные подсистемы и их взаимодействия, предоставляет
// простой интерфейс для работы с ними.
//
// Плюсы:
//   - клиенту не нужно знать о подсистемах
//
// Минусы:
//   - фасад может стать божественным объектом
type Service struct {
	repo        Repository
	validator   Validator
	notificator Notificator
}

func (s *Service) AddVal(id int, val int) error {
	if ok := s.validator.Validate(id); !ok {
		return errors.New("invalid id")
	}
	if err := s.repo.Add(id, val); err != nil {
		return err
	}
	msg := fmt.Sprintf("added new data %v by id %v", val, id)
	s.notificator.Notify(msg)
	return nil
}

func facadeClient(service *Service) {
	service.AddVal(10, 20)
}
