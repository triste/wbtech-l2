package main

import (
	"encoding/json"
	"fmt"
	"log"
	"net"
	"net/http"
	"os"
	"strconv"
	"sync"
	"time"
)

/*
=== HTTP server ===

Реализовать HTTP сервер для работы с календарем. В рамках задания необходимо работать строго со стандартной HTTP библиотекой.
В рамках задания необходимо:
	1. Реализовать вспомогательные функции для сериализации объектов доменной области в JSON.
	2. Реализовать вспомогательные функции для парсинга и валидации параметров методов /create_event и /update_event.
	3. Реализовать HTTP обработчики для каждого из методов API, используя вспомогательные функции и объекты доменной области.
	4. Реализовать middleware для логирования запросов
Методы API: POST /create_event POST /update_event POST /delete_event GET /events_for_day GET /events_for_week GET /events_for_month
Параметры передаются в виде www-url-form-encoded (т.е. обычные user_id=3&date=2019-09-09).
В GET методах параметры передаются через queryString, в POST через тело запроса.
В результате каждого запроса должен возвращаться JSON документ содержащий либо {"result": "..."} в случае успешного выполнения метода,
либо {"error": "..."} в случае ошибки бизнес-логики.

В рамках задачи необходимо:
	1. Реализовать все методы.
	2. Бизнес логика НЕ должна зависеть от кода HTTP сервера.
	3. В случае ошибки бизнес-логики сервер должен возвращать HTTP 503. В случае ошибки входных данных (невалидный int например) сервер должен возвращать HTTP 400. В случае остальных ошибок сервер должен возвращать HTTP 500. Web-сервер должен запускаться на порту указанном в конфиге и выводить в лог каждый обработанный запрос.
	4. Код должен проходить проверки go vet и golint.
*/

type InvalidInputError string

func (e *InvalidInputError) Error() string {
	return fmt.Sprintf("invalid input: %v", *e)
}

type BusinessLogicError string

func (e *BusinessLogicError) Error() string {
	return string(*e)
}

type NotFoundError string

func (e *NotFoundError) Error() string {
	return fmt.Sprintf("%v not found", *e)
}

type UserID = int
type EventID = int

type Event struct {
	ID     EventID   `json:"id"`
	UserID UserID    `json:"user_id"`
	Date   time.Time `json:"date"`
}

func (e *Event) Validate() error {
	if e.UserID < 0 {
		err := InvalidInputError("user id")
		return &err
	}
	if e.Date.IsZero() {
		err := InvalidInputError("date")
		return &err
	}
	return nil
}

type Period int

const (
	Day Period = iota + 1
	Week
	Month
)

type EventRepository struct {
	events map[EventID]Event
	nextID EventID
	mu     sync.RWMutex
}

func NewEventRepository() *EventRepository {
	return &EventRepository{
		events: make(map[EventID]Event),
	}
}

func (r *EventRepository) GetForPeriod(
	date time.Time,
	period Period,
	userID *UserID,
) ([]Event, error) {
	r.mu.RLock()
	defer r.mu.RUnlock()
	var events []Event
	week := date.Weekday()
	year, month, day := date.Date()
	for _, event := range r.events {
		y, m, d := event.Date.Date()
		switch period {
		case Day:
			if d != day {
				continue
			}
			fallthrough
		case Week:
			if event.Date.Weekday() != week {
				continue
			}
			fallthrough
		case Month:
			if m != month {
				continue
			}
			fallthrough
		default:
			if y != year {
				continue
			}
		}
		if userID != nil && *userID != event.UserID {
			continue
		}
		events = append(events, event)
	}
	return events, nil
}

func (r *EventRepository) Add(event *Event) {
	r.mu.Lock()
	defer r.mu.Unlock()
	event.ID = r.nextID
	r.nextID++
	r.events[event.ID] = *event
}

func (r *EventRepository) Update(event *Event) {
	r.mu.Lock()
	defer r.mu.Unlock()
	r.events[event.ID] = *event
}

func (r *EventRepository) GetByID(id EventID) (*Event, error) {
	r.mu.RLock()
	defer r.mu.RUnlock()
	if event, found := r.events[id]; found {
		return &event, nil
	}
	err := NotFoundError("event")
	return nil, &err
}

func (r *EventRepository) DeleteByID(id EventID) {
	r.mu.Lock()
	defer r.mu.Unlock()
	delete(r.events, id)
}

type Calendar struct {
	eventRepo *EventRepository
}

func NewCalendar(eventRepo *EventRepository) *Calendar {
	return &Calendar{
		eventRepo: eventRepo,
	}
}

func (c *Calendar) CreateEvent(userID UserID, date time.Time) (*Event, error) {
	event := &Event{
		UserID: userID,
		Date:   date,
	}
	if err := event.Validate(); err != nil {
		return nil, err
	}
	c.eventRepo.Add(event)
	return event, nil
}

func (c *Calendar) UpdateEvent(id EventID, date time.Time) (*Event, error) {
	event, err := c.eventRepo.GetByID(id)
	if err != nil {
		return nil, err
	}
	event.Date = date
	if err := event.Validate(); err != nil {
		return nil, err
	}
	c.eventRepo.Update(event)
	return event, nil
}

func (c *Calendar) DeleteEvent(id EventID) error {
	event, err := c.eventRepo.GetByID(id)
	if err != nil {
		return err
	}
	c.eventRepo.DeleteByID(event.ID)
	return nil
}

func (c *Calendar) ListEvents(userID *UserID, date *time.Time, period Period) ([]Event, error) {
	if date == nil {
		now := time.Now()
		date = &now
	}
	events, err := c.eventRepo.GetForPeriod(*date, period, userID)
	return events, err
}

type Controller struct {
	calendar *Calendar
}

func NewController(calendar *Calendar) *Controller {
	return &Controller{
		calendar: calendar,
	}
}

type ResultResponse struct {
	Result any `json:"result"`
}

type ErrorResponse struct {
	Error string `json:"error"`
}

func writeJson(w http.ResponseWriter, code int, obj any) {
	w.WriteHeader(code)
	w.Header().Add("Content-Type", "application/json")
	data, err := json.Marshal(obj)
	if err != nil {
		log.Fatal(err)
	}
	w.Write(data)
}

func writeResult(w http.ResponseWriter, obj any) {
	resp := ResultResponse{obj}
	writeJson(w, http.StatusOK, resp)
}

func writeError(w http.ResponseWriter, err error) {
	code := http.StatusInternalServerError
	switch err.(type) {
	case *InvalidInputError:
		code = http.StatusBadRequest
	case *NotFoundError:
		code = http.StatusNotFound
	case *BusinessLogicError:
		code = http.StatusServiceUnavailable // 503
	}
	resp := ErrorResponse{err.Error()}
	writeJson(w, code, resp)
}

type CreateEventRequest struct {
	UserID UserID
	Date   time.Time
}

func NewCreateEventRequest(r *http.Request) (*CreateEventRequest, error) {
	if err := r.ParseForm(); err != nil {
		return nil, err
	}
	userIdStr := r.FormValue("user_id")
	userID, err := strconv.Atoi(userIdStr)
	if err != nil {
		err := InvalidInputError("user id: " + err.Error())
		return nil, &err
	}
	dateStr := r.FormValue("date")
	date, err := time.Parse(time.DateOnly, dateStr)
	if err != nil {
		err := InvalidInputError("date: " + err.Error())
		return nil, &err
	}
	return &CreateEventRequest{
		UserID: userID,
		Date:   date,
	}, nil
}

type UpdateEventRequest struct {
	ID   EventID
	Date time.Time
}

func NewUpdateEventRequest(r *http.Request) (*UpdateEventRequest, error) {
	if err := r.ParseForm(); err != nil {
		return nil, err
	}
	dateStr := r.FormValue("date")
	date, err := time.Parse(time.DateOnly, dateStr)
	if err != nil {
		return nil, err
	}
	return &UpdateEventRequest{
		Date: date,
	}, nil
}

func (c *Controller) CreateEvent(w http.ResponseWriter, r *http.Request) {
	req, err := NewCreateEventRequest(r)
	if err != nil {
		writeError(w, err)
		return
	}
	event, err := c.calendar.CreateEvent(req.UserID, req.Date)
	if err != nil {
		writeError(w, err)
		return
	}
	writeResult(w, event)
}

func (c *Controller) UpdateEvent(w http.ResponseWriter, r *http.Request) {
	req, err := NewUpdateEventRequest(r)
	if err != nil {
		writeError(w, err)
		return
	}
	event, err := c.calendar.UpdateEvent(req.ID, req.Date)
	if err != nil {
		writeError(w, err)
		return
	}
	writeResult(w, event)
}

type DeleteEventRequest struct {
	ID EventID `json:"id"`
}

func NewDeleteEventRequest(r *http.Request) (*DeleteEventRequest, error) {
	if err := r.ParseForm(); err != nil {
		return nil, err
	}
	idStr := r.FormValue("id")
	id, err := strconv.Atoi(idStr)
	if err != nil {
		err := InvalidInputError("id: " + err.Error())
		return nil, &err
	}
	return &DeleteEventRequest{
		ID: id,
	}, nil
}

func (c *Controller) DeleteEvent(w http.ResponseWriter, r *http.Request) {
	req, err := NewDeleteEventRequest(r)
	if err != nil {
		writeError(w, err)
		return
	}
	if err := c.calendar.DeleteEvent(req.ID); err != nil {
		writeError(w, err)
		return
	}
	writeResult(w, req.ID)
}

type ListEventRequest struct {
	UserID *UserID    `json:"user_id"`
	Date   *time.Time `json:"date"`
}

type ListEventResponse struct {
	Events []Event `json:"events"`
}

func (c *Controller) ListEvents(w http.ResponseWriter, r *http.Request) {
	qvals := r.URL.Query()
	req := ListEventRequest{}
	userID := qvals.Get("user_id")
	if len(userID) > 0 {
		id, err := strconv.Atoi(userID)
		if err != nil {
			err := InvalidInputError("user id: " + err.Error())
			writeError(w, &err)
			return
		}
		req.UserID = &id
	}
	date := qvals.Get("date")
	if len(date) > 0 {
		date, err := time.Parse(time.DateOnly, date)
		if err != nil {
			err := InvalidInputError("date: " + err.Error())
			writeError(w, &err)
			return
		}
		req.Date = &date
	}

	var period Period
	switch r.URL.Path {
	case "/events_for_day":
		period = Day
	case "/events_for_week":
		period = Week
	case "/events_for_month":
		period = Month
	}
	events, err := c.calendar.ListEvents(req.UserID, req.Date, period)
	if err != nil {
		writeError(w, err)
		return
	}
	writeResult(w, ListEventResponse{events})
}

func NewRouter(ctrl *Controller) http.Handler {
	mux := http.NewServeMux()
	mux.HandleFunc("POST /create_event", ctrl.CreateEvent)
	mux.HandleFunc("POST /update_event", ctrl.UpdateEvent)
	mux.HandleFunc("POST /delete_event", ctrl.DeleteEvent)
	mux.HandleFunc("GET /events_for_day", ctrl.ListEvents)
	mux.HandleFunc("GET /events_for_week", ctrl.ListEvents)
	mux.HandleFunc("GET /events_for_month", ctrl.ListEvents)
	return mux
}

type codeRecorder struct {
	http.ResponseWriter
	code int
}

func (r *codeRecorder) WriteHeader(code int) {
	r.code = code
	r.ResponseWriter.WriteHeader(code)
}

func withLogger(handler http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		rec := codeRecorder{w, http.StatusNotImplemented}
		start := time.Now()
		handler.ServeHTTP(&rec, r)
		code := rec.code
		path := r.URL.Path
		query := r.URL.RawQuery
		ip := r.RemoteAddr
		userAgent := r.Header.Get("User-Agent")
		latency := time.Since(start)
		log.Printf("code: '%v', path: '%v', query: '%v', ip: '%v', agent: '%v', latency: '%v'",
			code, path, query, ip, userAgent, latency)
	})
}

func run(port string) error {
	repo := NewEventRepository()
	calendar := NewCalendar(repo)
	ctrl := NewController(calendar)
	router := NewRouter(ctrl)
	addr := net.JoinHostPort("localhost", port)
	server := http.Server{
		Addr:    addr,
		Handler: withLogger(router),
	}
	log.Printf("Starting server on %v port", port)
	return server.ListenAndServe()
}

func main() {
	port := os.Getenv("PORT")
	if err := run(port); err != nil {
		log.Fatal(err)
	}
}
