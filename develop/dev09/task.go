package main

/*
=== Утилита wget ===

Реализовать утилиту wget с возможностью скачивать сайты целиком

Программа должна проходить все тесты. Код должен проходить проверки go vet и golint.
*/

import (
	"bytes"
	"flag"
	"io"
	"log"
	"net/http"
	"net/url"
	"os"

	"golang.org/x/net/html"
)

func main() {
	flag.Parse()
	rootStr := flag.Arg(0)
	rootUrl, err := url.Parse(rootStr)
	if err != nil {
		log.Fatal(err)
	}
	visited := make(map[string]struct{})
	var dfs func(*url.URL)
	var walk func(*html.Node, *url.URL)
	walk = func(n *html.Node, u *url.URL) {
		if n.Type == html.ElementNode && n.Data == "a" {
			for _, a := range n.Attr {
				if a.Key == "href" {
					u, err := u.Parse(a.Val)
					if err != nil {
						log.Println(err)
					}
					dfs(u)
					break
				}
			}
		}
		for c := n.FirstChild; c != nil; c = c.NextSibling {
			walk(c, u)
		}
	}
	dfs = func(u *url.URL) {
		if u.Host != rootUrl.Host {
			return
		}
		if _, ok := visited[u.Path]; ok {
			return
		}
		visited[u.Path] = struct{}{}
		resp, err := http.Get(u.String())
		if err != nil {
			log.Println(u, err)
			return
		}
		defer resp.Body.Close()
		body, err := io.ReadAll(resp.Body)
		save(u, body)
		reader := bytes.NewReader(body)
		node, err := html.Parse(reader)
		if err != nil {
			log.Println(u, err)
			return
		}
		walk(node, u)
	}
	dfs(rootUrl)
}

func save(u *url.URL, body []byte) {
	path := u.Hostname() + u.Path
	filename := "index.html"
	os.MkdirAll(path, 0700)
	file, err := os.Create(path + "/" + filename)
	defer file.Close()
	if err != nil {
		log.Println(err)
		return
	}
	defer file.Close()
	reader := bytes.NewReader(body)
	_, err = io.Copy(file, reader)
	if err != nil {
		log.Println(err)
	}
}
