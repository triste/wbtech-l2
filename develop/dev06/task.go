package main

import (
	"bufio"
	"errors"
	"flag"
	"fmt"
	"log"
	"math"
	"os"
	"sort"
	"strconv"
	"strings"
)

/*
=== Утилита cut ===

Принимает STDIN, разбивает по разделителю (TAB) на колонки, выводит запрошенные

Поддержать флаги:
-f - "fields" - выбрать поля (колонки)
-d - "delimiter" - использовать другой разделитель
-s - "separated" - только строки с разделителем

Программа должна проходить все тесты. Код должен проходить проверки go vet и golint.
*/

func main() {
	var (
		fields    string
		delimiter string
		separated bool
	)
	flag.StringVar(&fields, "f", "", "select only these fields; also print any line that contains no delimiter character, unless the -s option is specified")
	flag.StringVar(&delimiter, "d", "\t", "field delimiter")
	flag.BoolVar(&separated, "s", false, "do not print lines not containing delimiters")
	flag.Parse()

	if len(fields) == 0 {
		log.Fatal("you must specify a list of fields")
	}
	intervals, err := parseFieldList(fields)
	if err != nil {
		log.Fatal(err)
	}

	filename := flag.Arg(0)
	var file *os.File
	if len(filename) == 0 {
		file = os.Stdin
	} else {
		var err error
		if file, err = os.Open(filename); err != nil {
			log.Fatal(err)
		}
	}
	scanner := bufio.NewScanner(file)
	var builder strings.Builder
	for scanner.Scan() {
		line := scanner.Text()
		if separated && !strings.Contains(line, delimiter) {
			continue
		}
		fields := strings.Split(line, delimiter)
	outer:
		for _, interval := range intervals {
			for i := interval[0] - 1; i < interval[1]; i++ {
				if i >= len(fields) {
					break outer
				}
				builder.WriteString(fields[i])
				builder.WriteString(delimiter)
			}
		}
		cuttedLine := builder.String()
		fmt.Println(cuttedLine)
		builder.Reset()
	}
	if err := scanner.Err(); err != nil {
		log.Fatal(err)
	}
}

func parseFieldList(list string) ([][2]int, error) {
	ranges := strings.Split(list, ",")
	var intervals [][2]int
	for _, rng := range ranges {
		start, end, err := parseRange(rng)
		if err != nil {
			return nil, err
		}
		intervals = append(intervals, [2]int{start, end})
	}
	sort.Slice(intervals, func(i, j int) bool {
		return intervals[i][0] < intervals[j][0]
	})
	var merged [][2]int
	for _, interval := range intervals {
		lastIdx := len(merged) - 1
		if lastIdx < 0 || merged[lastIdx][1] < interval[0] {
			merged = append(merged, interval)
		} else {
			merged[lastIdx][1] = max(merged[lastIdx][1], interval[1])
		}
	}
	return merged, nil
}

func parseRange(rng string) (int, int, error) {
	numsStr := strings.Split(rng, "-")
	if len(numsStr) == 0 || len(numsStr) > 2 {
		return 0, 0, fmt.Errorf("invalid field range '%v'", rng)
	}
	nums := make([]int, len(numsStr))
	for i, numStr := range numsStr {
		num, err := strconv.Atoi(numStr)
		if err != nil {
			return 0, 0, fmt.Errorf("invalid field value '%v'", rng)
		}
		nums[i] = num
	}
	var start, end int
	if strings.HasPrefix(rng, "-") {
		if len(nums) != 1 {
			return 0, 0, fmt.Errorf("invalid field range '%v'", rng)
		}
		start = 1
		end = nums[0]
	} else if strings.HasSuffix(rng, "-") {
		if len(nums) != 1 {
			return 0, 0, fmt.Errorf("invalid field range '%v'", rng)
		}
		start = nums[0]
		end = math.MaxInt
	} else if len(nums) == 1 {
		start = nums[0]
		end = nums[0]
	} else {
		start = nums[0]
		end = nums[1]
	}
	if start == 0 {
		return 0, 0, errors.New("fields are numbered from 1")
	}
	if end < start {
		return 0, 0, errors.New("invalid decreasing range")
	}
	return start, end, nil
}
