package main

import (
	"bufio"
	"container/list"
	"flag"
	"fmt"
	"log"
	"os"
	"regexp"
	"strings"
)

/*
=== Утилита grep ===

Реализовать утилиту фильтрации (man grep)

Поддержать флаги:
-A - "after" печатать +N строк после совпадения
-B - "before" печатать +N строк до совпадения
-C - "context" (A+B) печатать ±N строк вокруг совпадения
-c - "count" (количество строк)
-i - "ignore-case" (игнорировать регистр)
-v - "invert" (вместо совпадения, исключать)
-F - "fixed", точное совпадение со строкой, не паттерн
-n - "line num", печатать номер строки

Программа должна проходить все тесты. Код должен проходить проверки go vet и golint.
*/

func main() {
	var (
		after      int
		before     int
		context    int
		count      bool
		ignoreCase bool
		invert     bool
		fixed      bool
		lineNum    bool
	)
	flag.IntVar(&after, "A", 0, "Print N lines of trailing context after matching lines")
	flag.IntVar(&before, "B", 0, "Print  N lines  of  leading  context  before  matching  lines")
	flag.IntVar(&context, "C", 0, "Print N lines of output context")
	flag.BoolVar(&count, "c", false, "Suppress normal output; instead print a count of matching lines for each input file")
	flag.BoolVar(&ignoreCase, "i", false, "Ignore case distinctions in patterns and input data")
	flag.BoolVar(&invert, "v", false, "Invert the sense of matching")
	flag.BoolVar(&fixed, "F", false, "Interpret pattern as fixed strings, not regular expressions")
	flag.BoolVar(&lineNum, "n", false, "Prefix each line of output with the 1-based line number within its input file")
	flag.Parse()

	after = max(after, context)
	before = max(before, context)

	pattern := flag.Arg(0)
	if len(pattern) == 0 {
		log.Fatal("pattern required")
	}
	filename := flag.Arg(1)
	var file *os.File
	if len(filename) == 0 {
		file = os.Stdin
	} else {
		var err error
		if file, err = os.Open(filename); err != nil {
			log.Fatal(err)
		}
		defer file.Close()
	}
	if ignoreCase {
		pattern = strings.ToLower(pattern)
	}
	var rexp *regexp.Regexp
	if !fixed {
		var err error
		if rexp, err = regexp.Compile(pattern); err != nil {
			log.Fatal(err)
		}
	}
	scanner := bufio.NewScanner(file)
	prevLines := list.New()
	printAfterCount := 0
	lastPrintedIdx := 0
	matchCount := 0
	printLine := func(line string, idx int, delimiter byte) {
		if lineNum {
			fmt.Printf("%d%c%s\n", idx, delimiter, line)
		} else {
			fmt.Println(line)
		}
	}
	for i := 1; scanner.Scan(); i++ {
		lineOrig := scanner.Text()
		line := lineOrig
		if ignoreCase {
			line = strings.ToLower(line)
		}
		var match bool
		if fixed {
			match = line == pattern
		} else {
			match = rexp.MatchString(line)
		}
		if invert {
			match = !match
		}
		if match {
			if count {
				matchCount++
				continue
			}
			printAfterCount = after
			if i-lastPrintedIdx > prevLines.Len()+1 {
				fmt.Println("--")
			}
			for prevLines.Len() > 0 {
				el := prevLines.Front()
				printLine(el.Value.(string), i-prevLines.Len(), '-')
				prevLines.Remove(el)
			}
			printLine(lineOrig, i, ':')
			lastPrintedIdx = i
		} else if printAfterCount > 0 {
			printAfterCount--
			printLine(lineOrig, i, '-')
			lastPrintedIdx = i
		} else {
			prevLines.PushBack(lineOrig)
			if prevLines.Len() > before {
				prevLines.Remove(prevLines.Front())
			}
		}
	}
	if err := scanner.Err(); err != nil {
		log.Fatal(err)
	}
	if count {
		fmt.Println(matchCount)
	}
}
