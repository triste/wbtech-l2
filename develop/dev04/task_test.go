package main

import (
	"reflect"
	"testing"
)

func TestAnagramSets(t *testing.T) {
	scenarios := map[string]struct {
		input []string
		want  map[string][]string
	}{
		"Empty": {},
		"Keys": {
			input: []string{"пятак", "листок", "пятка", "слиток", "тяпка", "столик"},
			want: map[string][]string{
				"пятак":  {"пятак", "пятка", "тяпка"},
				"листок": {"листок", "слиток", "столик"},
			},
		},
		"Sort": {
			input: []string{"тяпка", "пятка", "пятак"},
			want: map[string][]string{
				"тяпка": {"пятак", "пятка", "тяпка"},
			},
		},
		"Register": {
			input: []string{"пяТак", "Пятка", "ТЯПКА"},
			want: map[string][]string{
				"пяТак": {"пятак", "пятка", "тяпка"},
			},
		},
		"Unique": {
			input: []string{"пяТак", "Пятка", "ТЯПКА", "пятка", "пятак", "тяпка"},
			want: map[string][]string{
				"пяТак": {"пятак", "пятка", "тяпка"},
			},
		},
		"Single": {
			input: []string{"тяпка", "пятка", "слиток", "пятак"},
			want: map[string][]string{
				"тяпка": {"пятак", "пятка", "тяпка"},
			},
		},
	}
	for name, scenario := range scenarios {
		t.Run(name, func(t *testing.T) {
			got := anagramSets(scenario.input)
			if !reflect.DeepEqual(got, scenario.want) {
				t.Errorf("got %v; want %v", got, scenario.want)
			}
		})
	}
}
