package main

import (
	"slices"
	"strings"
)

/*
=== Поиск анаграмм по словарю ===

Напишите функцию поиска всех множеств анаграмм по словарю.
Например:
'пятак', 'пятка' и 'тяпка' - принадлежат одному множеству,
'листок', 'слиток' и 'столик' - другому.

Входные данные для функции: ссылка на массив - каждый элемент которого - слово на русском языке в кодировке utf8.
Выходные данные: Ссылка на мапу множеств анаграмм.
Ключ - первое встретившееся в словаре слово из множества
Значение - ссылка на массив, каждый элемент которого, слово из множества. Массив должен быть отсортирован по возрастанию.
Множества из одного элемента не должны попасть в результат.
Все слова должны быть приведены к нижнему регистру.
В результате каждое слово должно встречаться только один раз.

Программа должна проходить все тесты. Код должен проходить проверки go vet и golint.
*/

func main() {

}

func anagramSets(words []string) map[string][]string {
	if len(words) == 0 {
		return nil
	}
	wordsSet := make(map[string]struct{})
	result := make(map[string][]string)
	firstWords := make(map[string]string)
	for _, word := range words {
		wordLower := strings.ToLower(word)
		if _, found := wordsSet[wordLower]; found {
			continue
		}
		wordsSet[wordLower] = struct{}{}
		runes := []rune(wordLower)
		slices.Sort(runes)
		sortedWord := string(runes)
		firstWord, found := firstWords[sortedWord]
		if !found {
			firstWords[sortedWord] = word
			firstWord = word
		}
		result[firstWord] = append(result[firstWord], wordLower)
	}
	for firstWord, anagrams := range result {
		if len(anagrams) == 1 {
			delete(result, firstWord)
		} else {
			slices.Sort(result[firstWord])
		}
	}
	return result
}
