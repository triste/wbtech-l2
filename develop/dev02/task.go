package main

import (
	"errors"
	"unicode"
	"unicode/utf8"
)

/*
=== Задача на распаковку ===

Создать Go функцию, осуществляющую примитивную распаковку строки, содержащую повторяющиеся символы / руны, например:
	- "a4bc2d5e" => "aaaabccddddde"
	- "abcd" => "abcd"
	- "45" => "" (некорректная строка)
	- "" => ""
Дополнительное задание: поддержка escape - последовательностей
	- qwe\4\5 => qwe45 (*)
	- qwe\45 => qwe44444 (*)
	- qwe\\5 => qwe\\\\\ (*)

В случае если была передана некорректная строка функция должна возвращать ошибку. Написать unit-тесты.

Функция должна проходить все тесты. Код должен проходить проверки go vet и golint.
*/

func main() {
}

var (
	errStartWithDigit = errors.New("packed string start with digit")
	errEndWithEscape  = errors.New("packed string end with escape")
)

func unpack(str string) (string, error) {
	if char, _ := utf8.DecodeRuneInString(str); unicode.IsDigit(char) {
		return "", errStartWithDigit
	}
	// Количество символов после распаковки >= количеству байт в исходной строке / 2
	chars := make([]rune, 0, len(str)/2)
	escaped := false
	repeatCount := 0
	for _, ch := range str {
		if escaped {
			chars = append(chars, ch)
			escaped = false
		} else if ch == '\\' {
			escaped = true
		} else if unicode.IsDigit(ch) {
			repeatCount = repeatCount*10 + int(ch-'0')
		} else {
			repeatLastChar(&chars, repeatCount-1)
			repeatCount = 0
			chars = append(chars, ch)
		}
	}
	if escaped {
		return "", errEndWithEscape
	}
	repeatLastChar(&chars, repeatCount-1)
	return string(chars), nil
}

func repeatLastChar(chars *[]rune, count int) {
	if count <= 0 {
		return
	}
	lastChar := (*chars)[len(*chars)-1]
	for range count {
		*chars = append(*chars, lastChar)
	}
}
