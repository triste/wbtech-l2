package main

import "testing"

func TestUnpack(t *testing.T) {
	scenarios := map[string]struct {
		input string
		want  string
		err   error
	}{
		"Empty": {},
		"StartWithDigit": {
			input: `45`,
			err:   errStartWithDigit,
		},
		"EndWithEscape": {
			input: `abcd\`,
			err:   errEndWithEscape,
		},
		"Identical": {
			input: `abcd`,
			want:  `abcd`,
		},
		"Repeats": {
			input: `a4bc2d5e`,
			want:  `aaaabccddddde`,
		},
		"EscapeDigits": {
			input: `qwe\4\5`,
			want:  `qwe45`,
		},
		"RepeatDigit": {
			input: `qwe\45`,
			want:  `qwe44444`,
		},
		"RepeatEscape": {
			input: `qwe\\5`,
			want:  `qwe\\\\\`,
		},
	}
	for name, scenario := range scenarios {
		t.Run(name, func(t *testing.T) {
			got, err := unpack(scenario.input)
			if err != scenario.err {
				t.Errorf("got %v; want %v", err, scenario.err)
			}
			if got != scenario.want {
				t.Errorf("got %v; want %v", got, scenario.want)
			}
		})
	}
}
