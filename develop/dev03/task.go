package main

import (
	"bufio"
	"errors"
	"flag"
	"fmt"
	"log"
	"os"
	"sort"
	"strconv"
	"strings"
)

/*
=== Утилита sort ===

Отсортировать строки (man sort)
Основное

Поддержать ключи

-k — указание колонки для сортировки
-n — сортировать по числовому значению
-r — сортировать в обратном порядке
-u — не выводить повторяющиеся строки

Дополнительное

Поддержать ключи

-M — сортировать по названию месяца
-b — игнорировать хвостовые пробелы
-c — проверять отсортированы ли данные
-h — сортировать по числовому значению с учётом суффиксов

Программа должна проходить все тесты. Код должен проходить проверки go vet и golint.
*/

type line struct {
	str string
	// cached field by which sorting is performed, union
	keyStr string
	keyNum int
}

func main() {
	var (
		key      int
		numeric  bool
		reversed bool
		unique   bool
	)
	flag.IntVar(&key, "k", 1, "sort via a key")
	flag.BoolVar(&numeric, "n", false, "compare according to string numerical value")
	flag.BoolVar(&reversed, "r", false, "reverse the result of comparisons")
	flag.BoolVar(&unique, "u", false, "output only the first of an equal run")
	flag.Parse()
	filename := flag.Arg(0)

	lines, err := buildLines(filename, key-1, numeric, unique)
	if err != nil {
		log.Fatal(err)
	}
	sortLines(lines, numeric)
	printLines(lines, reversed)
}

func buildLines(filename string, key int, numeric, unique bool) ([]line, error) {
	if key < 0 {
		return nil, errors.New("invalid key")
	}
	var lines []line
	var file *os.File
	if len(filename) == 0 {
		file = os.Stdin
	} else {
		var err error
		if file, err = os.Open(filename); err != nil {
			return nil, err
		}
		defer file.Close()
	}
	scanner := bufio.NewScanner(file)
	linesSet := make(map[string]struct{})
	for scanner.Scan() {
		str := scanner.Text()
		if unique {
			if _, found := linesSet[str]; found {
				continue
			}
			linesSet[str] = struct{}{}
		}
		fields := strings.Fields(str)
		var keyField string
		if key < len(fields) {
			keyField = fields[key]
		}
		line := line{
			str: str,
		}
		if numeric {
			num, err := strconv.Atoi(keyField)
			if err != nil {
				num = 0
			}
			line.keyNum = num
		} else {
			line.keyStr = keyField
		}
		lines = append(lines, line)
	}
	if err := scanner.Err(); err != nil {
		return nil, err
	}
	return lines, nil
}

func sortLines(lines []line, numeric bool) {
	var sortFun func(i, j int) bool
	if numeric {
		sortFun = func(i, j int) bool {
			return lines[i].keyNum < lines[j].keyNum
		}
	} else {
		sortFun = func(i, j int) bool {
			return lines[i].keyStr < lines[j].keyStr
		}
	}
	sort.Slice(lines, sortFun)
}

func printLines(lines []line, reverse bool) {
	if reverse {
		for i := len(lines) - 1; i >= 0; i++ {
			fmt.Println(lines[i].str)
		}
	} else {
		for _, line := range lines {
			fmt.Println(line.str)
		}
	}
}
