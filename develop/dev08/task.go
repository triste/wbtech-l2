package main

import (
	"bufio"
	"errors"
	"fmt"
	"io"
	"log"
	"os"
	"os/exec"
	"strconv"
	"strings"
	"sync"
)

/*
=== Взаимодействие с ОС ===

Необходимо реализовать собственный шелл

встроенные команды: cd/pwd/echo/kill/ps
поддержать fork/exec команды
конвеер на пайпах

Реализовать утилиту netcat (nc) клиент
принимать данные из stdin и отправлять в соединение (tcp/udp)
Программа должна проходить все тесты. Код должен проходить проверки go vet и golint.
*/

const (
	prompt = "sh$ "
)

func parse(line string) ([]command, error) {
	line = strings.TrimSpace(line)
	if len(line) == 0 {
		return nil, nil
	}
	strs := strings.Split(line, "|")
	cmds := make([]command, 0, len(strs))
	for _, str := range strs {
		args := strings.Fields(str)
		cmd, err := newCommand(args)
		if err != nil {
			return nil, err
		}
		cmds = append(cmds, cmd)
	}
	return cmds, nil
}

/*
cmd1 | cmd2 | cmd3

stdin   > cmd1 > pipe1_w
pipe1_r > cmd2 > pipe2_w
pipe2_r > cmd3 > stdout
*/

func main() {
	scanner := bufio.NewScanner(os.Stdin)
	for {
		fmt.Print(prompt)
		if !scanner.Scan() {
			break
		}
		// TODO: quotes
		line := scanner.Text()
		cmds, err := parse(line)
		if err != nil {
			fmt.Fprintln(os.Stderr, err)
		}
		var wg sync.WaitGroup
		var prevReader *os.File
		for i, cmd := range cmds {
			var newReader, writer *os.File
			if i < len(cmds)-1 {
				var err error
				newReader, writer, err = os.Pipe()
				if err != nil {
					log.Fatal(err)
				}
			}
			reader := prevReader
			prevReader = newReader
			wg.Add(1)
			go func(cmd command) {
				defer wg.Done()
				if reader == nil {
					reader = os.Stdin
				} else {
					defer reader.Close()
				}
				if writer == nil {
					writer = os.Stdout
				} else {
					defer writer.Close()
				}
				if err := cmd.execute(reader, writer); err != nil {
					fmt.Fprintln(os.Stderr, err)
				}
			}(cmd)
		}
		wg.Wait()
	}
	if err := scanner.Err(); err != nil {
		log.Fatal(err)
	}
}

type command interface {
	execute(in io.Reader, out io.Writer) error
}

// cd
type changeDirectoryCommand struct {
	path string
}

func newChangeDirectoryCommand(args []string) (command, error) {
	var path string
	switch len(args) {
	case 0:
		path = os.Getenv("HOME")
	case 1:
		path = args[0]
	default:
		return nil, errors.New("cd: too many arguments")
	}
	return &changeDirectoryCommand{
		path: path,
	}, nil
}

func (c *changeDirectoryCommand) execute(in io.Reader, out io.Writer) error {
	return os.Chdir(c.path)
}

// pwd
type printWorkingDirectoryCommand struct {
}

func newPrintWorkingDirectoryCommand(args []string) (command, error) {
	return &printWorkingDirectoryCommand{}, nil
}

func (c *printWorkingDirectoryCommand) execute(in io.Reader, out io.Writer) error {
	wd, err := os.Getwd()
	if err != nil {
		return err
	}
	fmt.Fprintln(out, wd)
	return nil
}

// echo
type echoCommand struct {
	args []string
}

func newEchoCommand(args []string) (command, error) {
	return &echoCommand{
		args: args,
	}, nil
}

func (c *echoCommand) execute(in io.Reader, out io.Writer) error {
	fmt.Fprintln(out, strings.Join(c.args, " "))
	return nil
}

// kill
type killCommand struct {
	pid int
}

func newKillCommand(args []string) (command, error) {
	if len(args) != 1 {
		return nil, errors.New("kill: usage: kill pid")
	}
	pid, err := strconv.Atoi(args[0])
	if err != nil {
		return nil, errors.New("kill: argument must be pid process")
	}
	return &killCommand{
		pid: pid,
	}, nil
}

func (c *killCommand) execute(in io.Reader, out io.Writer) error {
	process, err := os.FindProcess(c.pid)
	if err != nil {
		return err
	}
	return process.Kill()
}

// exit
type exitCommand struct {
	code int
}

func newExitCommand(args []string) (command, error) {
	if len(args) > 2 {
		return nil, errors.New("exit: too many arguments")
	}
	code := 0
	if len(args) != 0 {
		var err error
		if code, err = strconv.Atoi(args[0]); err != nil {
			return nil, err
		}
	}
	return &exitCommand{
		code: code,
	}, nil
}

func (c *exitCommand) execute(in io.Reader, out io.Writer) error {
	os.Exit(c.code)
	return nil
}

// exec
type execCommand struct {
	name string
	args []string
}

func newExecCommand(args []string) (command, error) {
	return &execCommand{
		name: args[0],
		args: args[1:],
	}, nil
}

func (c *execCommand) execute(in io.Reader, out io.Writer) error {
	cmd := exec.Command(c.name, c.args...)
	cmd.Stdin = in
	cmd.Stdout = out
	cmd.Stderr = os.Stderr
	if err := cmd.Start(); err != nil {
		return err
	}
	return cmd.Wait()
}

func newCommand(args []string) (command, error) {
	if len(args) == 0 {
		return nil, errors.New("zero arguments")
	}
	var newCommand func(args []string) (command, error)
	cmdArgs := args[1:]
	switch args[0] {
	case "cd":
		newCommand = newChangeDirectoryCommand
	case "pwd":
		newCommand = newPrintWorkingDirectoryCommand
	case "echo":
		newCommand = newEchoCommand
	case "kill":
		newCommand = newKillCommand
	case "exit":
		newCommand = newExitCommand
	default:
		newCommand = newExecCommand
		cmdArgs = args
	}
	cmd, err := newCommand(cmdArgs)
	return cmd, err
}
