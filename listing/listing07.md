Что выведет программа? Объяснить вывод программы.

```go
package main

import (
	"fmt"
	"math/rand"
	"time"
)

func asChan(vs ...int) <-chan int {
	c := make(chan int)

	go func() {
		for _, v := range vs {
			c <- v
			time.Sleep(time.Duration(rand.Intn(1000)) * time.Millisecond)
		}

		close(c)
	}()
	return c
}

func merge(a, b <-chan int) <-chan int {
	c := make(chan int)
	go func() {
		for {
			select {
			case v := <-a:
				c <- v
			case v := <-b:
				c <- v
			}
		}
	}()
	return c
}

func main() {

	a := asChan(1, 3, 5, 7)
	b := asChan(2, 4 ,6, 8)
	c := merge(a, b )
	for v := range c {
		fmt.Println(v)
	}
}
```

Ответ:
```
Пока оба канала не закрыты, выводятся последовательности в произвольном
порядке. Когда один из каналов закроется, выводятся нули и оставшаяся
последовательность, также в произвольном порядке. Когда оба станут закрыты,
выводятся бесконечно нули.

Кейс в select отрабатывается без блокировки если канал закрыт, выдывая
zero value, поэтому в канал merge не перестанут писаться данные.

Чтобы решить проблему, нужно в merge на каждый канал создать горутину и писать
в C данные из A/B до закрытия A/B. Также создать горутину закрывающую канал C после
ожидания остальных двух горутин.
```
